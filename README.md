./node_modules/.bin/webpack


# Minimal Website : React and NodeJs

![Illustration](http://img4.hostingpics.net/pics/638887ScreenShot20161006at13956PM.png)  

###Why  
>I started to work with react, just to learn something new, and I told myself I will try a new kind of backend with NodeJs.  
And the both, together, are really not easy to use. Consequently, I searched the minimum code required to make it work.   

###Requirements :  
- NodeJs  
- All the packages in the package.json file  

###Installation :  
1. git clone https://github.com/TBouder/minimal_website  
2. npm install  
3. ./start.sh  
